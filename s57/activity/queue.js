let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection
}



function enqueue(element) {
    //In this function you are going to make an algo that will add an element
    //to the array (last element)
    collection[collection.length]=element;
    return collection  
}



function dequeue() {
    // In here you are going to remove the first element in the array
    for(let i=0; i<collection.length-1; i++){
        collection[i]=collection[i+1];
    }
    collection.length--;
    return collection;
}



function front() {
    // you will get the first element
    return collection[0]
}


function size() {
     // Number of elements  
     let i=0;
     while(collection[i]!==undefined){
        i++
     } 
     return i
}



function isEmpty() {
    //it will check whether the function is empty or not
    let i=0;
    while(collection[i]!==undefined){
        i++;
    }
    return i===0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};